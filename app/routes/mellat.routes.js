const exress = require("express");

const controllers = require("../controllers/mellat.controllers");

const Router = exress.Router();

Router.post('/request', controllers.RequestPayment);
Router.post('/verification', controllers.Verification);

module.exports = Router;