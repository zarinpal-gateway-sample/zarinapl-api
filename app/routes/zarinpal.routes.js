const exress = require("express");

const controllers = require("../controllers/zarinpal.controllers");

const Router = exress.Router();

Router.post('/request', controllers.RequestPayment);
Router.get('/verification', controllers.Verification);
Router.get('/unverified', controllers.Unverification);

module.exports = Router;