const mongoose = require("mongoose");

const Schema = mongoose.Schema;

const zarinpalModel = new Schema(
    {
        Authority: {
            type: String,
            required: true,
            default: "",
        },
        Amount: {
            type: Number,
            required: true,
            default: "",
        },
        Description: {
            type: String,
            required: true,
            default: "",
        },
        Phone: {
            type: String,
            required: true,
            default: "",
        },
        RefID: {
            type: Number,
            required: false,
            default: 0,
        },
        Verified: {
            type: Boolean,
            required: false,
            default: false,
        }
    },
    { timestamps: true },
);

const Zarinpal = mongoose.model('zarinpal', zarinpalModel);

module.exports = Zarinpal;