const mongoose = require("mongoose");

const Schema = mongoose.Schema;

const mellatModel = new Schema(
    {
        OrderId: {
            type: String,
            required: true,
            default: "",
        },
        Amount: {
            type: Number,
            required: true,
            default: "",
        },
        RefId: {
            type: String,
            required: true,
            default: "",
        },
        Phone: {
            type: String,
            required: true,
            default: "",
        },
        Verified: {
            type: Boolean,
            required: false,
            default: false,
        },
        SaleOrderId: {
            type: String,
            required: false,
            default: "",
        },
        SaleReferenceId: {
            type: String,
            required: false,
            default: "",
        },
    },
    { timestamps: true },
);

const Mellat = mongoose.model('mellat', mellatModel);

module.exports = Mellat;