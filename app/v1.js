const express = require("express");

const zarinpalRoutes = require("./routes/zarinpal.routes");
const mellatRoutes = require("./routes/mellat.routes");

const app = express();

app.use('/payment/zarinpal', zarinpalRoutes);
app.use('/payment/mellat', mellatRoutes);

module.exports = app;