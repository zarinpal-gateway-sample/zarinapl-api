const Mellat = require("../models/mellat.model");

const MellatCheckout = require('mellat-checkout');
const PayamSMS = require("payamsms-sdk");

require("dotenv").config();
const env = process.env;

function makeid(length) {
    var result = '';
    var characters = '0123456789';
    var charactersLength = characters.length;

    for ( var i = 0; i < length; i++ ) {
        result += characters.charAt(
            Math.floor(
                Math.random() * charactersLength
            )
        );
    }

   return result;
}

const mellat = new MellatCheckout({
    terminalId: env.MELLAT_TERMINAL,
    username: env.MELLAT_USERNAME,
    password: env.MELLAT_PASSWORD,
    timeout: 1000000000,
    apiUrl: 'https://banktest.ir/gateway/bpm.shaparak.ir/pgwchannel/services/pgw?wsdl',
});
const sms = new PayamSMS(env.PAYAM_ORGANIZATION, env.PAYAM_USERNAME, env.PAYAM_PASSWORD, env.PAYAM_LINE);

mellat.initialize()
    .then((result) => console.log("Mellat is ready"))
    .catch((error) => console.log("Mellat is no ready"))

const RequestPayment = (req, res) => {
    const data = req.body;

    const orderId = makeid(10);

    mellat.paymentRequest({
        amount: data.Amount,
        orderId: orderId,
        callbackUrl: `${env.CALLBACK_URL}/v1/payment/mellat/verification`,
    })
        .then((response) => {
            if (response.resCode === 0) {
                const insertingData = {
                    Amount: data.Amount,
                    OrderId: orderId,
                    RefId: response.refId,
                    Phone: data.Phone,
                };
    
                const payment = new Mellat(insertingData);
    
                payment.save()
                    .then((result) => res.status(200).send(response))
                    .catch((error) => {
                        res.status(500).send({ message: "Can not create model", error });
                    });
            } else {
                res.status(400).send({ message: "Bank error", response })
            }
        })
        .catch((error) => {
            res.send(error);
        });
}

const Verification = (req, res) => {
    const { RefId, SaleOrderId, SaleReferenceId } = req.body;

    Mellat.findOneAndUpdate({ RefId }, { SaleOrderId, SaleReferenceId, Verified: true })
        .then((payment) => {
            mellat.verifyPayment({
                orderId: payment.orderId,
                saleOrderId: SaleOrderId,
                saleReferenceId: SaleReferenceId,
            })
                .then((response) => {
                    if (response.resCode === 0) {
                        const messages = [
                            {
                                body: "پرداخت شما با موفقیت انجام شد. با تشکر.",
                                recipient: payment.Phone,
                            }
                        ]

                        const send = sms.send(messages);

                        send.then((result) => {
                            if (result.code === 200) {
                                res.redirect(`${env.UI}/payment/success/${payment.RefId}`);
                            } else {
                                res.redirect(`${env.UI}/payment/failed`);
                            }
                        });
                    } else {
                        res.redirect(`${env.UI}/payment/failed`);
                    }
                })
                .catch((error) => {
                    res.status(400).send({ message: "Can not verify", error });
                });
        })
        .catch((error) => {
            res.status(500).send({ message: "Can not find model", error });
        })
}

module.exports = {
    RequestPayment,
    Verification,
}