const Zarinpal = require("../models/zarinpal.model");

const ZarinpalCheckout = require('zarinpal-checkout');
const PayamSMS = require("payamsms-sdk");

require("dotenv").config();
const env = process.env;

const sandbox = env.ZARINPAL_SANDBOX === "development" ? true : false;

var zarinpal = ZarinpalCheckout.create(env.ZARINPAL_MERCHANT, sandbox);
const sms = new PayamSMS(env.PAYAM_ORGANIZATION, env.PAYAM_USERNAME, env.PAYAM_PASSWORD, env.PAYAM_LINE);

const RequestPayment = (req, res) => {
    const { Amount, Phone } = req.body;

    zarinpal.PaymentRequest({
        Amount,
        Description: "Hello Zarinpal",
        CallbackURL: `${env.CALLBACK_URL}/v1/payment/zarinpal/verification`,
    })
        .then((response) => {
            if (response.status === 100) {
                const newPayment = new Zarinpal({
                    Amount,
                    Phone,
                    Description: "Hello Zarinpal",
                    Authority: response.authority,
                });

                newPayment.save()
                    .then((result) => res.status(200).send({ url: response.url }))
                    .catch((error) => res.status(500).send(error))
            }
            
            // {
            //     "status": 100,
            //     "authority": "A00000000000000000000000000372706119",
            //     "url": "https://www.zarinpal.com/pg/StartPay/A00000000000000000000000000372706119"
            // }
        })
        .catch((error) => {
            res.status(400).send({ message: "Can not create request", error });
        });
}

const Verification = (req, res) => {
    const { Authority, Status } = req.query;

    if (Status === "OK") {
        Zarinpal.findOne({ Authority })
            .then((payment) => {
                zarinpal.PaymentVerification({
                    Amount: payment.Amount,
                    Authority,
                })
                    .then((response) => {
                        if (response.status === 100) {
                            Zarinpal.findByIdAndUpdate(payment._id, { RefID: response.RefID, Verified: true })
                                .then((result) => {
                                    const messages = [
                                        {
                                            body: "پرداخت شما با موفقیت انجام شد. با تشکر.",
                                            recipient: result.Phone,
                                        }
                                    ]

                                    const send = sms.send(messages);
            
                                    send.then((result) => {
                                        if (result.code === 200) {
                                            res.redirect(`${env.UI}/payment/success/${payment.RefID}`);
                                        } else {
                                            res.redirect(`${env.UI}/payment/failed`);
                                        }
                                    });
                                })
                                .catch((error) => {
                                    res.redirect(`${env.UI}/payment/failed`);
                                })
                        } else {
                            res.redirect(`${env.UI}/payment/failed`);
                        }
            
                        // {
                        //     "status": 101,
                        //     "RefID": 37275657601
                        // }
                    })
                    .catch((error) => {
                        res.status(400).send({ message: "Can not verify", error });
                    });
            })
            .catch((error) => {
                res.send(error);
            });
    } else {
        res.redirect(`${env.UI}/payment/failed`);
    }
}

const Unverification = (req, res) => {
    zarinpal.UnverifiedTransactions()
        .then((response) => {
            res.status(200).send(response.authorities);
        })
        .catch((error) => {
            res.status(400).send({ message: "Can not show unverified", error });
        });
}

module.exports = {
    RequestPayment,
    Verification,
    Unverification,
}