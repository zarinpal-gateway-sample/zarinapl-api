# Zarinpal API

This is a **MERN** project that we are going to learn about **Zarinpal Payment Gateway**.

## Tech

### Back-End

API is created with **ExpressJs**.

### Front-End

Front-End is a **Reactjs** application.

[Link to ReactJs](https://gitlab.com/zarinpal-gateway-sample/zarinapl-frontend)

### Database

We use **MongoDB** as our database.

## How to run

### First clone and install deps

```bash
$ git clone https://gitlab.com/zarinpal-gateway-sample/zarinapl-api && cd zarinapl-api
$ npm i
```

### Then run application

```bash
$ npm test
```